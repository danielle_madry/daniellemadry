### Hi there ✋🏽


- 🔭 I’m currently working on building a portfolio and creating my website.
- 💻 I have a blog of my journey on [@Hashnode](https://browntechbae.hashnode.dev/)
- 🌱 I’m currently learning Web Development via the [@#100Devs](https://leonnoel.com/100devs/) cohort 2022.
- 👯 I’m looking to collaborate on Front-End Web Development Projects or Software Engineering projects in general.
- 🤔 I’m looking for help with anything I can, I have a plethora of resources.
- 💬 Ask me about Technology Podcast, Conferences,Web Development and Data.
- 📫 How to reach me: Email: Daniellemadry6@gmail.com or Twitter: [@BrownTechBae](https://twitter.com/browntechbae)
- 😄 Pronouns:She/Her💕
- ⚡ Fun fact: I have a Tech🎙Podcast called [BrownTechBae](https://anchor.fm/browntechbae). 
